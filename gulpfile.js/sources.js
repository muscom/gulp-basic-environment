const sources = (__target__) => {
    const conf = {
        type: {
            pug: {
                file: '*.pug',
                deny: '_*.pug',
            },
        },
        dir: {
            src: './src/**/',
            dest: './dist/',
        },
    };

    // Handle Exception
    const there_is = function () {
        if (!conf.type[__target__]) {
            throw `Error: ファイルタイプ ${__target__} は未定義です。type_conf.types に${__target__}の設定を追加してください。`;
        }
    };
    try {
        there_is(__target__);
    } catch (e) {
        console.error(e);
        return false;
    } finally {
        // Set file paths
        conf.path = {
            src: [conf.dir.src + conf.type[__target__].file],
            dest: [conf.dir.dest],
        };
        if (conf.type[__target__].deny) {
            conf.path.src.push('!' + conf.dir.src + conf.type[__target__].deny);
        }
        return conf.path;
    }
};
exports.sources = sources;
