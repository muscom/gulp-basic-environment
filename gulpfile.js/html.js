// Common
const { src, dest, watch, series, parallel } = require('gulp');
const replace = require('gulp-replace');

// Pug
const pug = require('gulp-pug');

// HTML Beautify
const htmlbeautify = require('gulp-html-beautify');

// Sources run test
const { sources } = require('./sources');

// Task
const compile_html = () => {
    const this_sources = sources('pug');
    return src(this_sources.src)
        .pipe(
            pug({
                basedir: '.',
            })
        )
        .pipe(
            htmlbeautify({
                indent_size: 4,
                end_with_newline: true,
            })
        )
        .pipe(dest(this_sources.dest));
};
exports.html = series(compile_html);
